#!/bin/bash

docker build -t "minechat" .
docker image save minechat | ssh dvmn-props-carpo "cat - | docker image load"

RUN_CMD="docker run -d --name minechat --restart always -p 0.0.0.0:5000:5000 -p 0.0.0.0:5050:5050 minechat"

ssh dvmn-props-carpo "docker stop minechat && docker rm minechat && $RUN_CMD"
echo 'Done'