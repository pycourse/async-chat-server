import asyncio
import itertools
import random
import uuid
import json
from dataclasses import dataclass, asdict
from contextlib import contextmanager
import os

from names import NAMES_ADJECTIVES
from bots_talks import DIALOG_1, DIALOG_2, DIALOG_3, DIALOG_4

hash2account = {}
broadcast_queues = []


def put2broadcast_queues(message):
    for queue in broadcast_queues:
        queue.put_nowait(message)


@contextmanager
def register_broadcaster():
    msgs2publish = asyncio.Queue()
    broadcast_queues.append(msgs2publish)
    try:
        yield msgs2publish
    finally:
        broadcast_queues.remove(msgs2publish)
        raise


@dataclass
class Message:
    """Class for keeping track of an item in inventory."""

    nickname: str
    text: str


@dataclass
class Account:
    """Class for keeping track of an item in inventory."""

    nickname: str
    account_hash: str


async def write_json(writer, data):
    writer.write(json.dumps(data, ensure_ascii=False).encode('utf-8'))
    writer.write('\n'.encode('utf-8'))
    await writer.drain()


async def write_text(writer, text):
    writer.write(text.encode('utf-8'))
    writer.write('\n'.encode('utf-8'))
    await writer.drain()


async def register_new_account(reader, writer):
    await write_text(writer, 'Enter preferred nickname below:')

    preferred_binary_nickname = await reader.readuntil()
    preferred_nickname = preferred_binary_nickname.decode('utf-8').strip()
    prefix = random.choice(NAMES_ADJECTIVES).title()  # used to prevent foreign username occupation
    nickname = '{} {}'.format(prefix, preferred_nickname)
    account_hash = str(uuid.uuid1())

    account = Account(nickname=nickname, account_hash=account_hash)
    hash2account[account_hash] = account

    return account


async def read_message_text(reader):
    msg_lines = []
    while True:
        binary_msg_line = await reader.readuntil()
        msg_line = binary_msg_line.decode('utf-8')
        if msg_line == '\n':
            break
        msg_lines.append(msg_line.strip('\n'))

    return '\n'.join(msg_lines)


async def talk_with_writer(reader, writer):
    await write_text(writer, 'Hello %username%! Enter your personal hash or leave it empty to create new account.')

    account_binary_hash = await reader.readuntil()
    account_hash = account_binary_hash.decode('utf-8').strip()

    account = hash2account.get(account_hash)
    new_user_joined = False
    if not account:
        if account_hash:
            await write_json(writer, None)
        account = await register_new_account(reader, writer)
        new_user_joined = True

    await write_json(writer, asdict(account))

    await write_text(writer, 'Welcome to chat! Post your message below. End it with an empty line.')

    while True:
        msg_text = await read_message_text(reader)

        msg = Message(account.nickname, msg_text)

        if new_user_joined:
            put2broadcast_queues(Message('Server', f'# к чату присоединился {account.nickname}'))
            new_user_joined = False

        if msg_text:
            put2broadcast_queues(msg)

        await write_text(writer, 'Message send. Write more, end message with an empty line.')


async def talk_with_writer_till_exit(reader, writer):
    try:
        await talk_with_writer(reader, writer)
    except (ConnectionResetError, asyncio.IncompleteReadError):
        pass



async def run_receiver_server(host, port):
    server = await asyncio.start_server(talk_with_writer_till_exit, host=host, port=port)

    async with server:
        sockname = server.sockets[0].getsockname()
        print(f'Recieving for new messages on {sockname}')
        await server.serve_forever()

    print('Messages receiving stopped')


async def broadcast_to_client(reader, writer):
    with register_broadcaster() as msgs2publish:
        while True:
            message = await msgs2publish.get()
            await write_text(writer, f'{message.nickname}: {message.text}')


async def broadcast_to_client_till_exit(reader, writer):
    try:
        await broadcast_to_client(reader, writer)
    except ConnectionResetError:
        pass


async def run_broadcast_server(host, port):
    server = await asyncio.start_server(broadcast_to_client_till_exit, host=host, port=port)

    async with server:
        sockname = server.sockets[0].getsockname()
        print(f'Broadcast on {sockname}')
        await server.serve_forever()

    print('Broadcast stopped')


async def chat_bots():
    for bot_name, message in itertools.cycle(itertools.chain(DIALOG_1, DIALOG_2, DIALOG_3, DIALOG_4)):
        await asyncio.sleep(random.random() * 3)
        put2broadcast_queues(Message(bot_name, message))


async def run_chat_servers():
    await asyncio.wait({
        run_broadcast_server(host='0.0.0.0', port=5000),
        run_receiver_server(host='0.0.0.0', port=5050),
        chat_bots(),
    })

if __name__ == '__main__':
    try:
        debug_mode = os.getenv('DEBUG_LOOP', '').lower() in ('1', 'yes')
        asyncio.run(run_chat_servers(), debug=debug_mode)
    except KeyboardInterrupt:
        pass
