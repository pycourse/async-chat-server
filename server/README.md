# How to run

```
docker-compose run  --service-ports  chat_server
```

Listen chat messages:

```
nc 127.0.0.1 5000
```

Send chat message:

```
nc 127.0.0.1 5050
```

# Proposals

pass