# How to run

Listen for chat messages:

```bash
python listener.py
```

Post message to chat:

```bash
python sender.py "Hello, everyone!"
```

New user will be created, after that you will receive account hash.

Post message and specify preferred username:

```bash
python sender.py -u pelid -a "2d2ad678-5618-11e9-853d-a7bd2b93a69c" "Hello! My name in @pelid"
```

Post message as already registered user:

```bash
python sender.py -a "2d2ad678-5618-11e9-853d-a7bd2b93a69c" "Hello! I've returned."
```


# Proposals

pass