import asyncio
import argparse

__all__ = (
    'read_msgs',
    'read_msgs_with_reconnect',
)


def create_argparser():
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('--host', default='127.0.0.1',
                        help='chat host to connect')
    parser.add_argument('--port', default='5000',
                        help='which port to listen for new messages')

    return parser


async def read_msgs(host, port):
    reader, writer = await asyncio.open_connection(host, port)

    try:
        while True:
            binary_line = await reader.readline()
            message = binary_line.decode('utf-8').strip('\n')
            yield message
    finally:
        writer.close()
        await writer.wait_closed()


async def read_msgs_with_reconnect(host, port):
    while True:
        try:
            async for message in read_msgs(host, port):
                yield message
        except (ConnectionResetError, ConnectionRefusedError):
            yield  # signals that connection lost


async def print_msgs(host, port):
    print('New messages:')

    reconnection_attempts = 0
    while True:
        async for message in read_msgs_with_reconnect(host, port):
            if message is None:
                reconnection_attempts += 1
                print(f'Connection lost. Reconnection attempt #{reconnection_attempts}')
                await asyncio.sleep(reconnection_attempts)
                continue
            elif reconnection_attempts:
                print(f'Connection repaired.')
                reconnection_attempts = 0

            print(message)


def main():
    args = create_argparser().parse_args()
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(print_msgs(args.host, args.port))
    except KeyboardInterrupt:
        pass


if __name__ == '__main__':
    main()
