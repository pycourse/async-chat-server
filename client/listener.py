import asyncio
import argparse


def create_argparser():
    parser = argparse.ArgumentParser(description='Receives new chat messages and list them on the screen.')
    parser.add_argument('--host', default='127.0.0.1',
                        help='chat host to connect')
    parser.add_argument('--port', default='5000',
                        help='which port to listen for new messages')

    return parser


async def print_msgs(host, port):

    reconnection_attempts = 0
    while True:
        try:
            reader, writer = await asyncio.open_connection(host, port)
        except ConnectionRefusedError:
            reconnection_attempts += 1
            print(f'Connection refused. Retry in {reconnection_attempts} sec')
            await asyncio.sleep(reconnection_attempts)
            continue

        try:
            while True:
                binary_line = await reader.readline()
                message = binary_line.decode('utf-8').strip('\n')

                if reconnection_attempts:
                    print(f'Connection established.')
                    reconnection_attempts = 0

                print(message)

        except ConnectionResetError:
            reconnection_attempts += 1
            print(f'Connection lost. Reconnection attempt #{reconnection_attempts}')
            await asyncio.sleep(reconnection_attempts)
            continue

        finally:
            writer.close()
            await writer.wait_closed()


def main():
    args = create_argparser().parse_args()
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(print_msgs(args.host, args.port))
    except KeyboardInterrupt:
        pass


if __name__ == '__main__':
    main()
