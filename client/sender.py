import asyncio
import json
import logging
import argparse

MESSAGE_END_SYMBOLS = '\n\n'

logger = logging.getLogger('sender')


def log_byte_line(binary_line):
    logger.debug(binary_line.decode('utf-8'))


def escape_message_text(text):
    non_empty_lines = [line or ' ' for line in text.splitlines()]
    return '\n'.join(non_empty_lines)


def escape_username(text):
    return text.replace('\n', ' ')


async def authorise(reader, writer, user_hash):
    writer.write(f'{user_hash}\n'.encode('utf-8'))
    await writer.drain()
    
    user_credentials_json = await reader.readline()
    log_byte_line(user_credentials_json)

    return json.loads(user_credentials_json)


async def read_prompt(reader):
    line = await reader.readline()
    log_byte_line(line)


async def register(reader, writer, preferred_username='anonymous'):
    writer.write(f'\n'.encode('utf-8'))
    await writer.drain()

    # select username
    log_byte_line(await reader.readline())
    escaped_username = escape_username(preferred_username)
    writer.write(f'{escaped_username}\n'.encode('utf-8'))
    await writer.drain()

    # receive credentials
    user_credentials_json = await reader.readline()
    return json.loads(user_credentials_json)


async def write_message(writer, text):
    escaped_text = escape_message_text(text)

    writer.write(f'{escaped_text}'.encode('utf-8'))
    writer.write(MESSAGE_END_SYMBOLS.encode('utf-8'))

    await writer.drain()


async def send_msg(host, port, text, user_hash=None, preferred_username='anonymous'):
    reader, writer = await asyncio.open_connection(host, port)

    try:
        await read_prompt(reader)

        if not user_hash:
            user_credentials = await register(reader, writer, preferred_username)

            await write_message(writer, text)

            print('Your new credentials:')
            print(json.dumps(user_credentials, indent=4))
        else:
            user_credentials = await authorise(reader, writer, user_hash)

            if not user_credentials:
                exit('User not found')

            await write_message(writer, text)
    finally:
        writer.close()
        await writer.wait_closed()

    return user_credentials


def create_argparser():
    parser = argparse.ArgumentParser(description='Receives new chat messages and list them on the screen.')
    parser.add_argument('message',
                        help='message to be sent')
    parser.add_argument('--host', default='127.0.0.1',
                        help='chat host to connect')
    parser.add_argument('--port', default='5050',
                        help='which port to send new message')
    parser.add_argument('--verbose', '-v', action='count')

    parser.add_argument('--account-hash', '-a',
                        help='account hash from user credentials')

    parser.add_argument('--username', '-u',
                        help='preferred username for chat')

    return parser


def main():
    args = create_argparser().parse_args()

    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)

    loop = asyncio.get_event_loop()
    send_coroutine = send_msg(args.host, args.port, args.message, user_hash=args.account_hash, preferred_username=args.username)
    loop.run_until_complete(send_coroutine)


if __name__ == '__main__':
    main()
